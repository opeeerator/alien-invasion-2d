# Alien-Invasion-2D
Alien Invasion game developed using pygame package in python.

<h4> Instructions </h4>
To run the game you can simply install pygame using pip and run the alien-invasion.py file!

<h4> Customize game settings </h4>
You can easily customize settings such as alien drop speed and etc by modifying the "settings.py" file.
<br> 
<br> 
<img src="https://gitlab.com/opeeerator/alien-invasion-2d/raw/master/game-view.PNG">

<i> This game is a project of a book called "python crash course" by eric matthes </i>

Have fun!
